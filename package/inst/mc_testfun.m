function [timeInSeconds, x] = mc_testfun(timeInSeconds, x=0)
  %TESTFUN  Do something stupid for specified amount of time
  %   [TIME, X] = TESTFUN(TIME, X) does some computations that take TIME seconds long
  %   and returns the inputs.
  %
  %		Markus Buehren
  %		Last modified 07.01.2009
  %		Stanislav Mašláň
  %		Last modified 27.11.2015
  %		Martin Šíra
  %		Last modified 13.02.2018

  % do something stupid that takes timeInSeconds seconds
  t0 = time;
  epsTime = 0.005;
  while time - t0 < timeInSeconds - epsTime
  	for k = 1:20
  		a = svd(rand(20)); %#ok
    end
  end
  %disp(sprintf('testfun took %.4f seconds.', time - t0));
endfunction

