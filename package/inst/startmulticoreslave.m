## Copyright (C) Markus Buehren 2011, Stanislav Mašláň 2015, Martin Šíra 2018
##

## -*- texinfo -*-
## @deftypefn {Function File} startmulticoreslave()
## @deftypefnx {Function File} startmulticoreslave(@var{multicoreDir})
## @deftypefnx {Function File} startmulticoreslave(@var{multicoreDir}, @var{settings})
## Start multi-core processing slave process. Function repeatedly
## checks the given directory @var{multicoreDir} for data
## files including which function to run and which parameters to use.
## If @var{multicoreDir} is not specified, standard directory
## ./multicore is used.
##
## The additional input structure @var{settings} may contain any of the
## following fields:
## @table @code
## @item startWaitTime
##   Set initial time to wait before checking for slave files (default value 0.1 s).
## @item maxWaitTime
## Set maximum time to wait before checking again after no slave
## file was found (default value is 2 s). This pause time prevents "busy waiting".
## @item maxIdleTime
##   If there are no slave files found for more than this time, the current
##   process will be shut down (default value Inf).
## @item firstWarnTime
##   Set time after which to notify the user if there are no slave files found (default 10 s).
## @item startWarnTime
##   Set first time after which to repeat the message about absence of slave files (default 600 s).
## @item maxWarnTime
##   Set maximum time after which to repeat the message about absence of slave files (default 86400 s).
## @item debugMode
## Activate/deactivate debug messages (default 0). Note: This option is for development, 
## other settings are overwritten if debugMode is activated!
## @item showWarnings
## Activate/deactivate additional warnings (default 0). Note: This
## option is for development.
## @end table
## @seealso{startmulticoremaster, runmulticore}
## @end deftypefn

function startmulticoreslave(multicoreDir, settings)

  % Default settings/parameters. 
  % Note: Use second input argument to overwrite these settings.
  
  % Set initial and maximum time to wait before checking again after no slave
  % file was found. This pause time prevents "busy waiting".
  settingsDefault.startWaitTime = 0.1; % in seconds
  settingsDefault.maxWaitTime   = 2.0; % in seconds
  
  % If there are no slave files found for more than this time, the current
  % Matlab process will be shut down (thanks Richard!).
  settingsDefault.maxIdleTime = inf; % in seconds
  
  % set time after which to notify the user if there are no slave files found
  settingsDefault.firstWarnTime = 10; % in seconds
  
  % set first and maximum time after which to repeat the message about
  % absence of slave files
  settingsDefault.startWarnTime = 10 * 60;   % in seconds
  settingsDefault.maxWarnTime   = 24 * 3600; % in seconds
  
  % Activate/deactivate debug messages and additional warnings. Note: These
  % options are for development, other settings are overwritten below if
  % debugMode is activated!
  settingsDefault.debugMode    = 0;
  settingsDefault.showWarnings = 0;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
  % combine default and user-provided settings/parameters
  if ~exist('settings', 'var')
    % use default settings
    settings = settingsDefault;
  else
    settings = mc_combineSettings(settings, settingsDefault);
  end
  
  % settings in debug mode
  if settings.debugMode
    settings.showWarnings  = 1;
    settings.firstWarnTime = 10;
    settings.startWarnTime = 10;
    settings.maxWarnTime   = 60;
    settings.maxWaitTime   = 1;
    warning('on', 'multicore:dir-stat');
    warning('on', 'multicore:dir-lstat');
  else
    % disable warnings which happens when job files disappears during reading content of directory:
    warning('off', 'multicore:dir-stat');
    warning('off', 'multicore:dir-lstat');
  end
  
  % get slave file directory name
  if ~exist('multicoreDir', 'var') || isempty(multicoreDir)
    multicoreDir = fullfile('.', 'multicorefiles');
  end
  if ~exist(multicoreDir, 'dir')
    try
      mkdir(multicoreDir);
    catch
      error('Unable to create slave file directory %s.', multicoreDir);
    end
  end
  
  % initialize variables
  lastEvalEndClock = clock;
  lastWarnClock    = clock;
  firstRun         = true;
  curWarnTime      = settings.firstWarnTime;
  curWaitTime      = settings.startWaitTime;
  
  % randomize manually, becasue sometimes the slaves get initialized to the same seed,
  % which results in colliding job files selection - drastically slows down the whole cluster of slaves
  rand('seed',getpid());
    
  persistent lastSessionDateStr;
  % paths to remove when new jobs batch detected:
  userPathsToRemove = {};
  
  % no parameter files found yet
  parameterFileList = {};
  parameterCount = 0;
  
  while 1
    
    %% search for job files again?
    if(~parameterCount)

      % yaha, search parameter files
      parameterFileList = mc_findfiles(multicoreDir, 'parameters_*.mat', 'nonrecursive');
            
      % reset file list
      parameterCount = numel(parameterFileList);
      parameterId = 0;
      
      % randomize list
      if(parameterCount)
        rnd_ids = randperm(parameterCount);        
        parameterFileList(:) = parameterFileList(rnd_ids);  
      endif
            
    endif
                   
    % get file from list
    parameterFileName = '';
    if(parameterCount)
      parameterFileName = parameterFileList{++parameterId};
      parameterCount--;
    endif
    
    %% get some parameter file from list and try to process it 
    if(numel(parameterFileName))           

      if settings.debugMode
        % get parameter file number for debug messages
        [a,b,c,d,stdid] = regexpi(parameterFileName,'parameters_[\d]+_([\d]+)[^\\\/]*$'); fileNr = str2num(stdid{1}{1});   
        
        fprintf('****** Slave is checking file nr %d *******\n', fileNr);
      end
  
      % try to get exclusive access to the parameter file
      [parameterFileName_tmp,p_tmp_ok] = mc_try_get_params_file(parameterFileName);
        
  
      % load and delete parameter file
      loadSuccessful = true;
      if p_tmp_ok && mc_existfile(parameterFileName_tmp)
        % try to load the parameters
        lastwarn('');
        lasterror('reset');
        clear functionHandlesStr functionHandles parameters;
        try
          load('-binary',parameterFileName_tmp,'parameters','functionHandlesStr','functionHandles','userPaths','CellExpansion'); %% file access %%
        catch
          % something went wrong
          if (exist('parameters','var') && numel(parameters)) && (exist('functionHandlesStr','var') && numel(functionHandlesStr)) && (~exist('functionHandles','var') || ~numel(functionHandles))
            % missing function handles: the absolute function path in the function handle variable is likely not valid
            %                           try to find the function(s) in search path and rebuild function handle(s)
            [functionHandles,funs_ok] = mc_rebuild_function_handles(functionHandlesStr);
            
            % check success
            if ~funs_ok
              loadSuccessful = false;
              fprintf('Warning: Unable to rebuild function handles in new location.\n');
              lastMsg = lastwarn;
              if ~isempty(lastMsg)
                fprintf('Warning message issued when trying to load:\n%s\n', lastMsg);
              end              
              mc_displayerrorstruct;              
            endif
            
          else
            % general fail          
            loadSuccessful = false;
            if settings.showWarnings
              fprintf('Warning: Unable to load parameter file %s.\n', parameterFileName_tmp);
              lastMsg = lastwarn;
              if ~isempty(lastMsg)
                fprintf('Warning message issued when trying to load:\n%s\n', lastMsg);
              end
              mc_displayerrorstruct;
            end
          endif
        end
  
        % check if variables to load are existing
        if loadSuccessful && (~exist('functionHandles','var') || ~numel(functionHandles) || ~exist('parameters','var') || ~numel(parameters))
          loadSuccessful = false;
          if settings.showWarnings
            disp(sprintf(['Warning: Either variable ''%s'' or ''%s''', ...
              'or ''%s'' not existing after loading file %s.'], ...
              'functionHandles', 'parameters', parameterFileName_tmp));
          end
        end
  
        if settings.debugMode
          if loadSuccessful
            fprintf('Successfully loaded parameter file nr %d.\n', fileNr);
          else
            fprintf('Problems loading parameter file nr %d.\n', fileNr);
          end
        end
  
        % remove parameter file
        [err,msg] = unlink(parameterFileName_tmp);      
        if err
          % If deletion is not successful it can happen that other slaves or
          % the master also use these parameters. To avoid this, ignore the
          % loaded parameters
          loadSuccessful = false;
          if settings.debugMode
            fprintf('Problems deleting parameter file nr %d. It will be ignored.\n', fileNr);
          end
        end
      else
        loadSuccessful = false;
        if settings.debugMode
          disp('No parameter files found.');
        end
      end
  
      % remove semaphore and continue if loading was not successful
      if ~loadSuccessful
        continue;
      end
  
      % show progress info
      if firstRun
        fprintf('First function evaluation (%s)\n', datestr(clock, 'mmm dd, HH:MM'));
        firstRun = false;
      elseif etime(clock, lastEvalEndClock) > 60
        fprintf('First function evaluation after %s (%s)\n', ...
          mc_formattime(etime(clock, lastEvalEndClock)), datestr(clock, 'mmm dd, HH:MM'));
      end
  
      %%%%%%%%%%%%%%%%%%%%%
      % evaluate function %
      %%%%%%%%%%%%%%%%%%%%%
      if settings.debugMode
        fprintf('Slave evaluates job nr %d.\n', fileNr);
        t0 = time();
      end
  
      % Check if date string in parameter file name has changed. If yes, call
      % "clear functions" to ensure that the latest file versions are used,
      % no older versions in Matlab's memory.
      [a,b,c,d,stdid] = regexpi(parameterFileName,'parameters_(\d+)_\d+\.mat'); sessionDateStr = stdid{1}{1};
      if ~strcmp(sessionDateStr, lastSessionDateStr)
        % clear functions
        clear functions;
        
        % remove old paths from last jobs batch:
        for k = 1:numel(userPathsToRemove)
          try
            rmpath(userPathsToRemove{k});
          end      
        endfor
        
        % add new paths defined by user:
        for k = 1:numel(userPaths)
          addpath(userPaths{k});      
        endfor        
        % rember paths that were added so they can be removed when new jobs batch detected:
        userPathsToRemove = userPaths;
        
        disp('New job batch detected - function cache cleared.');        
      end
      lastSessionDateStr = sessionDateStr;
  
      % process jobs in the file
      result = cell(size(parameters)); %#ok
      for k=1:numel(parameters)
        if iscell(parameters{k}) && CellExpansion
          result{k} = feval(mc_getFunctionHandleSlave(functionHandles, k), parameters{k}{:}); %#ok
        else
          result{k} = feval(mc_getFunctionHandleSlave(functionHandles, k), parameters{k}); %#ok
        end
      end
      if settings.debugMode
        fprintf('Slave finished job nr %d in %.2f seconds.\n', fileNr, time() - t0);
      end
  
      % save results to temp file
      resultFileName = strrep(parameterFileName, 'parameters', 'result');
      resultFileName_tmp = [resultFileName '.tmp'];
      try
        save('-binary',resultFileName_tmp,'result'); %% file access %%
        if settings.debugMode
          fprintf('Result file nr %d generated.\n', fileNr);
        end
      catch
        if settings.showWarnings
          fprintf('Warning: Unable to save file %s.\n', resultFileName_tmp);
          mc_displayerrorstruct;
        end
      end
      
      % activate result file
      [err,msg] = rename(resultFileName_tmp,resultFileName);
  
      % save time
      lastEvalEndClock = clock;
      curWarnTime = settings.startWarnTime;
      curWaitTime = settings.startWaitTime;

      % remove variables before next run
      %clear result functionHandles functionHandleStr parameters;      
functionHandles = {};
functionHandleStr = {};
result = {};
parameters ={};

  
    else
      % display message or exit if idle for long time
      timeSinceLastEvaluation = etime(clock, lastEvalEndClock);
  
      % exit slave process if idle for a long time
      if timeSinceLastEvaluation > settings.maxIdleTime
        fprintf('No slave files found during last %s (%s).\n', ...
          mc_formattime(timeSinceLastEvaluation), datestr(clock, 'mmm dd, HH:MM'));
        disp('Exiting MATLAB in ten seconds.');
        pause(10);
        quit force;
      end
  
      if min(timeSinceLastEvaluation, etime(clock, lastWarnClock)) > curWarnTime
        if timeSinceLastEvaluation >= 10*60
          % round to minutes
          timeSinceLastEvaluation = 60 * round(timeSinceLastEvaluation / 60);
        end
        disp(sprintf('Warning: No slave files found during last %s (%s).', ...
        mc_formattime(timeSinceLastEvaluation), datestr(clock, 'mmm dd, HH:MM')));
        lastWarnClock = clock;
        
        if firstRun
          curWarnTime = settings.startWarnTime;
        else
          curWarnTime = min(curWarnTime*2, settings.maxWarnTime);
        end        
      endif  
            
      % increase wait time
      curWaitTime = min(curWaitTime + 0.1,settings.maxWaitTime);
        
      % wait before next check
      pause(curWaitTime);
  
    end
  end
endfunction






