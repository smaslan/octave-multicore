%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Stanislav Mašláň
%		Last modified 27.11.2015
function [fHandles,fStr] = mc_getFunctionHandles(functionHandleCell, index)  
  
  if isa(functionHandleCell, 'function_handle')
    % return function handle as it is
    fHandles = functionHandleCell;
    fStr = func2str(fHandles);
  elseif iscell(functionHandleCell) 
    if all(size(functionHandleCell) == [1 1])
      % return function handle
      fHandles = functionHandleCell{1};
      fStr = func2str(fHandles);
    else
      if numel(index) == 1
        % return function handle
        fHandles = functionHandleCell{index};
        fStr = func2str(fHandles);       
      else
        % return function handle cell
        fHandles = functionHandleCell(index);
        fStr = cell(size(index));
        for k = 1:numel(index)
          fStr{k} = func2str(fHandles{k}); 
        endfor
      end
    end
  else
    error('Input type unknown.');
  end
  
endfunction % function
