%% this function tries to get exclusive access to paramter file
%		Stanislav Mašláň
%		Last modified 27.11.2015

function [pname_tmp,ok] = mc_try_get_params_file(pname)
    
  % generate random string
  [num,rstr] = mc_generaterandomnumber();
  
  % build temp file name
  pname_tmp = [pname '_' rstr '.tmp'];
  
  % try to rename file to temp file
  [err,msg] = rename(pname,pname_tmp);
  
  % leave if failed, some another process already used the file
  if(err || ~mc_existfile(pname_tmp))
    ok = 0;
    return;
  endif
  
  % now this process has exclusive access to the file
  ok = 1;
    
endfunction
