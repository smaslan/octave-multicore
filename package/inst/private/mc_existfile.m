function res = mc_existfile(fileName)
  %EXISTFILE  Check for file existence.
  %	  RES = EXISTFILE(FILENAME) returns true if the file FILENAME is existing
  %	  on the current path and false otherwise. It works similar to
  %	  EXIST(FILENAME, 'file'), except that it does not find files on the
  %	  Matlab path!
  %
  %		Example:
  %		existfile('test.txt');
  %
  %		Markus Buehren
  %		Last modified 03.02.2008 
  %		Stanislav Mašláň
  %		Last modified 27.11.2015
  % 
  %   See also EXIST.
  
  res = ~isempty(fileName) && exist(fileName,'file') ~= 0;

endfunction
