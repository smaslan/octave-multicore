%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Stanislav Mašláň
%		Last modified 27.11.2015

function settings = mc_combineSettings(settings, settingsDefault)

  % get settings
  if ~isstruct(settings)
    error('Input argument "settings" must be a struct.');
  else
    % check if there are unknown field names in struct settings
    fieldNames = fieldnames(settings);
    for k=1:length(fieldNames)
      if ~isfield(settingsDefault, fieldNames{k})
        error('Setting "%s" unknown.', fieldNames{k});
      end
    end
  
    % set default values where fields are missing in struct settings
    fieldNames = fieldnames(settingsDefault);
    for k=1:length(fieldNames)
      if ~isfield(settings, fieldNames{k})
        settings.(fieldNames{k}) = settingsDefault.(fieldNames{k});
      end
    end
  end

endfunction % function
