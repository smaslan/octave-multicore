%             Stanislav Mašláň
%		Last modified 27.11.2015

function [str] = mc_seconds_to_str(t)

  if(t<60)
    str = [int2str(t) 's'];
  elseif(t<60*60)
    str = sprintf('%d:%02.0f',floor(t/60),round(mod(t,60)));
  elseif(t<60*60*24)
    str = sprintf('%d:%02.0f:%02.0f',floor(t/3600),floor(mod(t,3600)/60),round(mod(t,60)));
  else
    str = sprintf('%d days %d:%02d:%0.02f',floor(t/3600/24),floor(mod(t,3600*24)/60/60),floor(mod(t,3600)/60),round(mod(t,60)));
  endif

endfunction
