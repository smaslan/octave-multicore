function fileCell = mc_findfiles(varargin)
  %MC_FINDFILES  Recursively search directory for files.
  %   MC_FINDFILES returns a cell array with the file names of all files
  %   in the current directory and all subdirectories.
  %
  %   MC_FINDFILES(DIRNAME) returns the file names of all files in the given
  %   directory and its subdirectories.
  %
  %   MC_FINDFILES(DIRNAME, FILESPEC) only returns the file names matching the
  %   given file specification (like '*.c' or '*.m').
  %
  %   MC_FINDFILES(DIRNAME, FILESPEC, 'nonrecursive') searches only in the top
  %   directory.
  %
  %   MC_FINDFILES(DIRNAME, FILESPEC, EXLUDEDIR1, ...) excludes the additional
  %   directories from the search.
  %
  %		Example:
  %		fileList = mc_findfiles('.', '*.m');
  %
  %		Markus Buehren
  %		Last modified 21.04.2008 
  %		Stanislav Mašláň
  %		Last modified 27.11.2015
  %
  %   See also DIR.
  
  % disable warnings
  %warning('off',[],'local');
    
  if nargin == 0
  	searchPath = '.';
  	fileSpec   = '*';
  elseif nargin == 1
  	searchPath = varargin{1};
  	fileSpec   = '*';
  else
  	searchPath = varargin{1};
  	fileSpec   = varargin{2};
  end
  
  excludeCell = {};
  searchrecursive = true;
  for n=3:nargin
  	if isequal(varargin{n}, 'nonrecursive')
  		searchrecursive = false;
  	elseif iscell(varargin{n})
   		excludeCell = [excludeCell, varargin{n}]; %#ok
  	elseif ischar(varargin{n}) && isdir(varargin{n})
  		excludeCell(n+1) = varargin(n); %#ok
  	else
  		error('Directory not existing or unknown command: %s', varargin{n});
  	end
  end
  
  searchPath = mc_chompsep(searchPath);
  if strcmp(searchPath, '.')
   	searchPath = '';
  elseif ~exist(searchPath, 'dir')
  	error('Directory %s not existing.', searchPath);
  end
  
  % initialize cell
  fileCell = {};
  
  % search for files in current directory
  dirStruct = mc_dir(mc_concatpath(searchPath, fileSpec));
  for n=1:length(dirStruct)
  	if ~dirStruct(n).isdir
  		fileCell(end+1) = {mc_concatpath(searchPath, dirStruct(n).name)}; %#ok
  	end
  end
  
  % search in subdirectories
  if searchrecursive
  	excludeCell = [excludeCell, {'.', '..', '.svn'}];
  	if isempty(searchPath)
  		dirStruct = mc_dir('.');
  	else
  		dirStruct = mc_dir(searchPath);
  	end
  	
  	for n=1:length(dirStruct)
  		if dirStruct(n).isdir
  			name = dirStruct(n).name;
  			if ~any(strcmp(name, excludeCell))
  				fileCell = [fileCell, mc_findfiles(mc_concatpath(searchPath, name), fileSpec)]; %#ok
  			end
  		end
  	end
  end
endfunction
