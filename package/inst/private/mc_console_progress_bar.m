%             Stanislav Mašláň
%		Last modified 4.5.2023

function [] = mc_console_progress_bar(files_N=0,files_proc_m=0,files_proc_s=0)

    persistent t_start = 0;
    persistent proc_list = [];
    persistent time_list = [];
    
    
    % initial time
    if ~t_start || ~files_N
          t_start = time();
          proc_list = [];
          time_list = [];
    endif
    
    % current time
    t_cur = time();
    
    % total processed files
    files_proc = files_proc_m + files_proc_s;
    
    time_list(end+1) = t_cur;
    proc_list(end+1) = files_proc;
    
       
      
    % update?
    if files_N && files_proc
    
        if files_proc >= 5
        
            max_n = 20;
            
            pp = polyfit(proc_list(max(2,end-max_n):end), time_list(max(2,end-max_n):end),1);
            t_end = polyval(pp,files_N);
                        
%             figure(1);
%             plot(proc_list, time_list - time_list(1), 'x');
%             hold on;
%             fit_N = [0:files_N];
%             t_fit = polyval(pp,fit_N);
%             plot(fit_N, t_fit - time_list(1), 'r');
%             hold off;
    
            
        else      
            % estimated finish time
            t_end = t_start + files_N*(t_cur-t_start)/files_proc;
        endif
        
        % remaining        
        t_remain = t_end - t_cur;
        
        % print status
        t_end_str = strftime('%k:%M:%S %Y-%m-%d',localtime(t_end));            
        fprintf('MC progress ... %0.1f%%, END = %s (T = -%s)                   \r',100*files_proc/files_N,t_end_str,mc_seconds_to_str(t_remain));
    else
        fprintf('MC progress ... %0.1f%%\r',0);
    end	  
  
endfunction
