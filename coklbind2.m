## Copyright (C) 2018 Martin Šíra
##

## -*- texinfo -*-
## @deftypefn {Function File} coklbind2 (@var{pids}, [@var{verbose}])
## Assigns processes to free cores. Usefull only on supercomputer cokl.
## First a `numabind` is run to find free cores. Next `taskset` is used to 
## assign processes with process ids @var{pids} to a free cores.
## @var{verbose} is verbosity level.
##

## Author: Martin Šíra <msiraATcmi.cz>
## Created: 2018
## Version: 0.1
## Keywords: cellfun parcellfun multicore
## Script quality:
##   Tested: yes
##   Contains help: yes
##   Contains example in help: no
##   Contains tests: no
##   Contains demo: no
##   Checks inputs: no
##   Optimized: no

function coklbind2(pids, verbose = 0)
        pause(1)
        procno = length(pids);
        if verbose disp(['Number of processes to assign: ' num2str(procno)]) endif

        % "numabind --offset" causes problems, it does not really works, it must be done in different way.
        % So this is outdated version:
        %       command = ["numabind --offset " num2str(procno) " --flags=best 2>/dev/null"];
        %       [sstat, sout] = system(command);
        %       freecores = str2num(sout);
        % One has to use "numabind --verbose". It takes too long time, so it is generated for all
        % every 15 minuts. Output of this is stored here:
        %       /ramfs/numabind_verbose.txt
        % And processed results are stored here:
        %       /ramfs/busy_cores.txt
        %       /ramfs/free_boards.txt
        %       /ramfs/num_free_cores.txt
        numabindverbose = fileread("/ramfs/numabind_verbose.txt");
        [S, E, TE, M, T, NM, SP] = regexp (numabindverbose, '\n(\d+) cores with {');
        numcores = str2num(T{1}{1});
        numfreecores = load("/ramfs/num_free_cores.txt");
        busycores = load("/ramfs/busy_cores.txt");
        freecores = setdiff([0:numcores-1], busycores);

        if length(freecores) ~= numfreecores
                error('Incorrectly loaded files or inconsistent data. Cannot determine free cores.')
        endif

        if verbose
                disp('List of free cores:')
                disp(freecores)
        endif

        if length(freecores) < procno
                error('Not enough empty cores!')
        endif

        for i = 1:procno
                command = ["taskset -pc " num2str(freecores(i)) " " num2str(pids(i))];
                [sstat, sout] = system(command);
        endfor
        
        % lower priotities:
        command = ["renice 5 -p " num2str(pids(:)',' %.0f')];
        [sstat, sout] = system(command);
        
endfunction

% vim settings modeline: vim: foldmarker=%<<<,%>>> fdm=marker fen ft=octave textwidth=1000
